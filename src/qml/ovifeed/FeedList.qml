import QtQuick 1.0
//import Analytics 1.0
import QtWebKit 1.0
import com.nokia.symbian 1.0
import Qt.labs.components.native 1.0

//import com.nokia.symbian 1.0


Window {
    id: mainWindow

    StatusBar {
        id: statusbar
        anchors.top: parent.top
    }

    ToolBar {
        id: toolBar
        anchors { bottom: parent.bottom; left: parent.left; right: parent.right }
//        tools: ToolBarLayout {
//            id: toolBarButtons
//            ToolButton {
//                iconSource: "toolbar-back"
//            }
//            ToolButton {
//                iconSource: "toolbar-menu"
//                //onClicked: launchMenu()
//            }
//            ToolButton {
//                iconSource: "toolbar-quit"
//                text: "Close"
//                onClicked: Qt.quit()
//            }
//        }
        //opacity: largeImagePage.chromeOpacity
    }
Rectangle {
    id: window
    //width: 480; height: 800     // maemo, meego
    width: 360;   height: 640     // E7, N8
    anchors { top: statusbar.bottom; }

    color: "white"

    property bool loading: feedModel.status == XmlListModel.Loading

    state: "showingSplashScreen"

    SystemPalette { id: activePalette }
    /*
    Analytics {
        id: analyticsID
        connectionTypePreference: Analytics.AnyConnection  // optional
        appLanguage: "En"                                  // optional
        minBundleSize: 20                                  // optional
        loggingEnabled: true                               // optional
    }

    // initialize and start analytics
    Component.onCompleted: {
        // Initialize item with \e {Application key} and \e {Agent name} values.
        analyticsID.initialize("7c66022d4a1ce83bfaab5345da89eca1", "1.0");
        // Start gathering analytics events (associated with a certain screen or app view).
        analyticsID.start("someScreen");
    }

    // stop analytics
    Component.onDestruction: {
        // Stop gathering analytics events => subsequent "logEvent" calls won't be registered.
        analyticsID.stop("someScreen", Analytics.AppExit);
    }
    */

    XmlListModel {
        id: feedModel
        //source: "../ovifeed/feed.xml"
        source: "http://feeds.ovi.com/output/xml?line=top50apps&group=RU_ru&tag=Yandextest"
        query: "/banners/banner"

        XmlRole { name: "name"; query: "name/string()" }
        XmlRole { name: "description"; query: "description/string()" }
        XmlRole { name: "icon_list"; query: "assets/asset[3]/url/string()" }              // list icon
        XmlRole { name: "icon_descp"; query: "assets/asset[3]/url/string()" }             // description icon
        XmlRole { name: "category"; query: "metadata/categories/category/category/category/@displayname/string()" }
        XmlRole { name: "price"; query: "price/string()" }
        //XmlRole { name: "ration"; query: "title/string()" }
        XmlRole { name: "url"; query: "url/string()" }
    }

    // splash screen
    SplashScreen { id: splashScreen; anchors.fill:  parent; z: 100 }

    // banner
    Banner { id: banner; anchors { top: window.top; } }

    // progress bar
    FeedProgressBar {
        scale: 1
        on: window.loading
        anchors { verticalCenter: parent.verticalCenter; horizontalCenter: parent.horizontalCenter }
    }


    // detail form
    DetailsScreen {
        id: dS;

        // descp image
        Image {
            id: descpIcon
            anchors { top: dS.top; topMargin: 20; left: dS.left; leftMargin: 10; }
        }

        // descp text
        Text {
            id: descpText
            //lineCount: 4
            anchors { top: descpIcon.bottom; topMargin: 10; left: dS.left; leftMargin: 10; }
            font { /*bold: true;*/ family: "Helvetica"; pointSize: 6 } color: "black"
        }

        // button Download
        Rectangle {
            id: downloadBtn; width: 160; height: 60; color: "#1E90FF"; radius: 28
            property string appUrl: ""; //border { color: "black"; width: 2 }
            anchors { top: descpText.bottom; topMargin: 130; left: dS.left; leftMargin: 10 }

            //Image { id: newgame; anchors.fill: parent; source: "../ovifeed/images/download.png" }
            Text {
                id: downloadText; text: "Download"; anchors { centerIn: parent }
                font { family: "Helvetica"; pointSize: 20 } color: "white"
            }

            // color the button with a gradient
//            gradient: Gradient {
//                GradientStop {
//                    position: 0.0
//                    color: {
//                        if (downloadArea.pressed)
//                            return activePalette.dark
//                        else
//                            return activePalette.light
//                    }
//                }
//                GradientStop { position: 1.0; color: activePalette.button }
//            }

            MouseArea {
                id: downloadArea;
                anchors.fill: parent;
                onClicked: {
                    console.log("download this");
                    Qt.openUrlExternally(downloadBtn.appUrl);
                }
            }
        }

        // button Facebook like
        Rectangle {
            id: facebookBtn
            property string appUrl: ""
            width: 32; height: 32
            anchors { top: downloadBtn.bottom; topMargin: 20; left: dS.left; leftMargin: 10 }
            color: "white"

            Image { id: facebookIcon; anchors.fill: parent; source: "../ovifeed/images/facebook.png" }

            MouseArea {
                anchors.fill: parent;
                onClicked: {
                    console.log("facebook like");
                    Qt.openUrlExternally("http://www.facebook.com/plugins/like.php?href=" + facebookBtn.appUrl)
                }
            }
        }

        // button Twitter like
        Rectangle {
            id: twitterBtn; width: 32; height: 32; color: "white"
            anchors { top: downloadBtn.bottom; topMargin: 20; left: facebookBtn.right; leftMargin: 20 }

            Image { id: twitterIcon; anchors.fill: parent; source: "../ovifeed/images/twitter.png" }

            MouseArea {
                anchors.fill: parent;
                onClicked: {
                    console.log("twitter like");
                    Qt.openUrlExternally("http://www.twitter.com/share")
                }
            }
        }

        WebView {
            id: webview
            property string message: ""
            // url: "http://www.nokia.com"
            preferredWidth: 360
            preferredHeight: 640
            scale: 1.0
            smooth: false
            opacity: 0

            settings.javascriptCanAccessClipboard : true
            settings.javascriptCanOpenWindows : true
            settings.javascriptEnabled : true
            settings.localContentCanAccessRemoteUrls : true

            onAlert: console.log(message)
        }

        // button Vkontakte like
        Rectangle {
            id: vkontakteBtn
            property string appUrl: ""
            property string appName: ""
            width: 32; height: 32
            anchors { top: downloadBtn.bottom; topMargin: 20; left: twitterBtn.right; leftMargin: 20 }
            color: "white"

            Image { id: vkontakteIcon; anchors.fill: parent; source: "../ovifeed/images/vk.png" }

            MouseArea {
                anchors.fill: parent;
                onClicked: {
                    console.log("vkontakte like");

                    backToDetailBtn.opacity = 1;

                    descpIcon.opacity = 0;
                    descpText.opacity = 0;
                    facebookBtn.opacity = 0;
                    twitterBtn.opacity = 0;
                    vkontakteBtn.opacity = 0;
                    odnoklasnikiBtn.opacity = 0;
                    downloadBtn.opacity = 0;

                    webview.opacity = 1;

                    // Проверить настройки браузера, жду ответа от Регины.
                    //*
                    webview.html =
                        "<head>" +
                            "<script type=\"text/javascript\" src=\"http://userapi.com/js/api/openapi.js?34\"></script>" +
                        "</head>" +
                        "<script type=\"text/javascript\">" +
                            "VK.init({apiId: 2494177, onlyWidgets: true});" +
                        "</script>" +
                        "<body>" +
                            ""+ vkontakteBtn.appName +" like button:" +
                            "</br>" +
                            "<div id=\"" + vkontakteBtn.appName.toLowerCase().replace(" ", "_") + "\"></div>" +
                            //"<div id=\"ovifeed_like\"></div>" +
                            "<script type=\"text/javascript\">" +
                                "VK.Widgets.Like(\"" + vkontakteBtn.appName.toLowerCase().replace(" ", "_") + "\", {type: \"full\"});" +
                                //"VK.Widgets.Like(\"ovifeed_like\", {type: \"full\"});" +
                            "</script>" +
                            "" + vkontakteBtn.appName.toLowerCase().replace(" ", "_") + "" +
                        "</body>";
                    //*/

                    // var keyID = genKey();
                    //"<script type=\"text/javascript\">" +
                    //"VK.init({apiId: 121454, onlyWidgets: true});" + // по apiID скрипт понимает, какая ссылка зарегана
                    //"</script>" +
                    //"<meta property=\"vk:url\" content=" + vkontakteBtn.appUrl + "/>" +
                    //* http://vkontakte.ru/js/api/openapi.js
                    // http://userapi.com/js/api/openapi.js?34
                    /*
                    webview.html =
                        "<head>" +
                            "<script type=\"text/javascript\" src=\"http://userapi.com/js/api/openapi.js?34\"></script>" +
                        "</head>" +
                        "<script type=\"text/javascript\">" +
                            "VK.init({apiId: 2494177, onlyWidgets: true});" +
                        "</script>" +
                        "<body>" +
                            ""+ vkontakteBtn.appName +" like button:" +
                            "</br>" +
                            //"<div id=\"" + vkontakteBtn.appName.toLowerCase().replace(" ", "_") + "\"></div>" +
                            "<div id=\"ovifeed_like\"></div>" +
                            "<script type=\"text/javascript\">" +
                                //"VK.Widgets.Like(\"" + vkontakteBtn.appName.toLowerCase().replace(" ", "_") + "\", {type: \"full\"});" +
                                "VK.Widgets.Like(\"ovifeed_like\", {type: \"full\"});" +
                            "</script>" +
                            "" + vkontakteBtn.appName.toLowerCase().replace(" ", "_") + "" +
                        "</body>";
                        //"<body> " + vkontakteBtn.appName.toLowerCase().replace(" ", "_") + " </body>";
                    */
                    /*
                    webview.html = "<head>" +
                            "<script type=\"text/javascript\" src=\"http://userapi.com/js/api/openapi.js?20\"></script>" +
                            "</head>" +
                            //"<script type=\"text/javascript\">" +
                            //"VK.init({apiId: API_ID, onlyWidgets: true});" + // по apiID скрипт понимает, какая ссылка зарегана
                            //"</script>" +
                            //"<meta property=\"vk:url\" content=" + vkontakteBtn.appUrl + "/>" +
                            "<div id=" + vkontakteBtn.appName.toLowerCase().replace(" ", "_") + "></div>" +
                            "<script type=\"text/javascript\">" +
                            "window.onload = function () {" +
                            "VK.init({apiId: 2494177, onlyWidgets: true});" +
                            "VK.Widgets.Like(" + vkontakteBtn.appName.toLowerCase().replace(" ", "_") + ", {type: \"full\", width: 500});}" +
                            "</script>" +
                            "<body>Fuck yeah ...</body>";
                    */
                    //webview.url = "http://vkontakte.ru/share.php?url=" + vkontakteBtn.appUrl;
                    //Qt.openUrlExternally("http://vkontakte.ru/share.php?url=" + vkontakteBtn.appUrl)    // share
                    //var id = 29465;
                    //console.log(id);
                    //VKLogic.init({apiId: 29465, onlyWidgets: true});
                    //VKLogic.VK.Widgets.Like("vk_like", {type: "button"});

                    console.log("after vkontakte like");
                }
            }
        }

        // button Odnoklasniki like
        Rectangle {
            id: odnoklasnikiBtn; width: 32; height: 32; color: "white"
            property string appUrl: ""
            anchors { top: downloadBtn.bottom; topMargin: 20; left: vkontakteBtn.right; leftMargin: 20 }

            Image { id: odnoklasnikiIcon; anchors.fill: parent; source: "../ovifeed/images/odnoklassniki.png" }

            MouseArea {
                anchors.fill: parent;
                onClicked: {
                    console.log("odnoklasniki like");

                    backToDetailBtn.opacity = 1;

                    descpIcon.opacity = 0;
                    descpText.opacity = 0;
                    facebookBtn.opacity = 0;
                    twitterBtn.opacity = 0;
                    vkontakteBtn.opacity = 0;
                    odnoklasnikiBtn.opacity = 0;
                    backToMainBtn.opacity = 0;
                    downloadBtn.opacity = 0;

                    webview.opacity = 1;
                    webview.html =
                            "<head>" +
                                "<link href=\"http://stg.odnoklassniki.ru/share/odkl_share.css\" rel=\"stylesheet\">" +
                                "<script src=\"http://stg.odnoklassniki.ru/share/odkl_share.js\" type=\"text/javascript\"></script>" +
                            "</head>" +
                            "<div style=\"float: center;\">" +
                                "</br>" +
                                "" + odnoklasnikiBtn.appName +"<br>" +
                                "<a class=\"odkl-klass-oc\" href=" + odnoklasnikiBtn.appUrl + " onclick=\"ODKL.Share(this);return false;\"><span>0</span></a>" +
                            "</div>" +
                            "<body onload=\"ODKL.init();\"></body>";


                    console.log("after odnoklasniki like");
                }
            }
        }

        // button Back to Main
        Rectangle {
            id: backToMainBtn
            width: 64; height: 64
            anchors { bottom: parent.bottom; bottomMargin: 20; left: dS.left; leftMargin: 10 }
            radius: 5; color: "transparent"

            Image { id: backMainIcon; anchors.fill: parent; source: "../ovifeed/images/back.png" }

            MouseArea {
                anchors.fill: parent;
                onClicked: {
                    console.log("back to main list");

                    dS.opacity = 0; window.opacity = 1;
                    appList.opacity = 1; banner.opacity = 1;
                }
            }
        }

        // button Back to Detail
        Rectangle {
            id: backToDetailBtn
            width: 64; height: 64
            anchors { bottom: parent.bottom; bottomMargin: 20; left: dS.left; leftMargin: 10 }
            radius: 5; color: "transparent"
            opacity: 0

            Image { id: backDetailIcon; anchors.fill: parent; source: "../ovifeed/images/back.png" }

            MouseArea {
                anchors.fill: parent;
                onClicked: {
                    console.log("back to detail");

                    webview.opacity = 0;
                    backToDetailBtn.opacity = 0;
                    descpIcon.opacity = 1;
                    descpText.opacity = 1;
                    facebookBtn.opacity = 1;
                    twitterBtn.opacity = 1;
                    vkontakteBtn.opacity = 1;
                    odnoklasnikiBtn.opacity = 1;
                    backToMainBtn.opacity = 1;
                    downloadBtn.opacity = 1;

                    webview.url = "";
                }
            }
        }
    }

    Component {
        id: listDelegate
        Rectangle {
            width: 460; height: 150
            color: "transparent"

            Image {
                id: appIcon
                source: icon_list

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        console.log("click: " + index);

                        appList.currentIndex = index;
                        appList.opacity = 0;
                        banner.opacity = 0;

                        appName.color = "purple";
                        appCPR.color = "purple";

                        //
                        descpIcon.source = feedModel.get(index).icon_descp;
                        descpText.text = feedModel.get(index).description;

                        // likes
                        downloadBtn.appUrl = feedModel.get(index).url;
                        facebookBtn.appUrl = feedModel.get(index).url;
                        vkontakteBtn.appUrl = feedModel.get(index).url;
                        vkontakteBtn.appName = feedModel.get(index).name;
                        odnoklasnikiBtn.appUrl = feedModel.get(index).url;
                        //odklText.appUrl = feedModel.get(index).url;

                        // Log activity event with the given screen.
                        //analyticsID.logEvent("someScreen", "appActivityEvent", Analytics.ActivityLogEvent);
                        // Log error event associated with the given screen (app view).
                        //analyticsID.logEvent("someScreen", "appErrorEvent", Analytics.ErrorLogEvent);

                        dS.show();
                    }
                }
            }

            Text {
                id: appName;
                anchors { top: appIcon.bottom; topMargin: 3 }
                text: name; color: "black"
                font { bold: true; family: "Helvetica"; pointSize: 8; }
            }
            Text {
                id: appCPR;
                anchors { top: appName.bottom; topMargin: 3 }
                text: category + ", " + price; color: "grey"
                font { /*bold: true;*/ family: "Helvetica"; pointSize: 6 }
            }
        }
    }

    // выделение элемента списка
    Component {
        id: highlightBar
        Rectangle {
            width: 440; height: 155
            radius: 5;
            border { width: 2; color: "red" }
            color: "transparent"
            y: appList.currentItem.y;
            x: appList.currentItem.x - 3;
            Behavior on y { PropertyAnimation {} }
        }
    }

    ListView {
        id: appList
        width: 360; height: 600; z: 2
        //anchors.fill: parent
        anchors { left: parent.left; leftMargin: 20; top: banner.bottom; topMargin: 15 }
        model: feedModel
        delegate: listDelegate

        //highlight: highlightBar
        //highlightFollowsCurrentItem: false
        //focus: true
        spacing: 5
        snapMode: ListView.SnapToItem
        //highlightRangeMode: ListView.ApplyRange
    }

    states: [
        State {
            name: "showingSplashScreen"
            PropertyChanges {
                target: splashScreen
                show: true
            }
        }
    ]
}
}
