import QtQuick 1.0


Rectangle {
    id: splash

    property int timeout: 3000  // 3 sec
    property string image: "../ovifeed/images/splash.png"
    property bool show: false

    signal splashTimeout()

    state: show ? "showingSplashScreen" : ""

    color: "white"
    opacity: 0.0

    onStateChanged: {
        if (state == "showingSplashScreen" )
            splashTimer.start();
    }

    Image {
        source: image
        anchors.centerIn: parent
    }

    Timer {
        id: splashTimer
        interval: timeout
        running: false
        repeat: false;
        onTriggered: { splash.splashTimeout(); splash.show = false }
   }

    states: [
        State {
            name: "showingSplashScreen"
            PropertyChanges {
                target: splash
                // We use opacity so we can animate, instead of visible
                opacity: 1.0
            }
        }
    ]

    transitions: [
        Transition {
            from: "showingSplashScreen"
            to: ""
            // Set this reversible if you would like to have also fading in.
            // Though it might look bit stupid during startup.
            //reversible: true

            // Animate the opacity change in 0,5s
            PropertyAnimation {
               property: "opacity"
               duration: 500
            }
        }
    ]
}
