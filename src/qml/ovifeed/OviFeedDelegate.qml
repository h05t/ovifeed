import QtQuick 1.0

Item {
    id: feedDelegate
    //width: 100
    //height: 62

    Row {
        Image {
            id: appIcon; source: icon
            MouseArea {
                id: iconArea
                anchors.fill: parent
                onClicked: { detailsScreen.show(); }
            }
        }
        Text {
            id: appText
            text: name + "\n" + description + "\n" + category + ", " + price + "\n"
        }
    }
}
