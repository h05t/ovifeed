import QtQuick 1.0

Rectangle {
     id: banner

     property int pixelSize: banner.height * 1.25
     property color textColor: "grey"
     property string text: "Лучшие приложения для Symbian             "

     width: 360; height: 40
     anchors { horizontalCenter: window.horizontalCenter }
     color: "white";

     Row {
         y: -banner.height / 13.5
         width: 360
         NumberAnimation on x { from: 0; to: -text.width ; duration: 10000; loops: Animation.Infinite }
         Text { id: text; font.pixelSize: 30; color: banner.textColor; text: banner.text }
         Text { color: banner.textColor; font.pixelSize: 30; text: banner.text; styleColor: "#ee1818" }
         Text { font.pixelSize: 30; color: banner.textColor; text: banner.text }
     }
 }
