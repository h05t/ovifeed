import QtQuick 1.0

Rectangle {
    id: detailsScreen
    width: 360; height: 640
    color: "white"

    opacity: 0
    //visible: opacity > 0

    signal closed
    signal opened

    function close() {
        if(detailsScreen.opacity == 0)  //already closed
            return;
        detailsScreen.closed();
        detailsScreen.opacity = 0;
    }

    function show() {
        detailsScreen.opened();
        detailsScreen.opacity = 1;
    }
}
