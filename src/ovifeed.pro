# Add more folders to ship with the application, here
folder_01.source = qml/ovifeed
folder_01.target = qml
DEPLOYMENTFOLDERS = folder_01

# Additional import path used to resolve QML modules in Creator's code model
QML_IMPORT_PATH =

QT += declarative
CONFIG += qt mobility
MOBILITY += multimedia systeminfo

#LIBS += -lanalytics

symbian: {
    TARGET.UID3 = 0xEFFCB920

    # set needed Symbian capabilities
    TARGET.CAPABILITY = NetworkServices LocalServices ReadUserData WriteUserData UserEnvironment ReadDeviceData

    # set Symbian maximum heap size to 5.5 MB
    TARGET.EPOCHEAPSIZE = 0x020000 0x580000

    # Define dependency to the In-App Analytics package
    # NOTE: Change In-App Analytics version number when needed currently this example uses 3.0.1
    analytics_deployment.pkg_prerules = "(0x20031574), 3, 0, 3, {\"In-App Analytics\"}"
    DEPLOYMENT += analytics_deployment
}

# The .cpp file which was generated for your project. Feel free to hack it.
SOURCES += main.cpp

# Please do not modify the following two lines. Required for deployment.
include(qmlapplicationviewer/qmlapplicationviewer.pri)
qtcAddDeployment()

OTHER_FILES +=
