#include "qmlapplicationviewer.h"
//#include "analyticshelper.h"
#include <QObject>
#include <QApplication>
#include <QDeclarativeView>
#include <QDeclarativeEngine>
#include <QDeclarativeContext>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QmlApplicationViewer viewer;
    //viewer.rootContext()->setContextProperty("analyticsHelper", new AnalyticsHelper());
    viewer.setOrientation(QmlApplicationViewer::ScreenOrientationAuto);
    viewer.setMainQmlFile(QLatin1String("qml/ovifeed/FeedList.qml"));
    viewer.showExpanded();

    return app.exec();
}
